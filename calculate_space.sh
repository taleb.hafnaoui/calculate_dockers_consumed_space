####################################################################################
#TALEB Hafnaoui 
#24/06/2022
#script pour le calcule espace de stockage par chaque docker
#
###################################################################################
#fonction de conversion de unité de mesure
function_name () {
arg1=$1

awk '
{
last_char=tolower(substr($0,length($0)))
switch(last_char){
  case "k":
    sum+=($0+0)/(1024*1024)
    break
  case "m":
    sum+=($0+0)/(1024)
    break
  case "g":
    sum+=$0+0
    break
  case "b":
    sum+=($0+0)/(1024*1024*1024)
    break
  case "?":
    print "Line is not matching anything here...."
}
}
END{
  print sum " G"
}'  $1

}
#########################################
#Fonction pour avoir la liste des clients
##########################################
list_client (){
clients=$(docker container ls | awk '{print $NF}' | sed '1d')
for client in $clients
  do
      docker exec $client /bin/sh -c "cd /exploit" 2&>  /dev/null
         if [ $? == 0 ]; then
            LIST="${LIST}\n""\n""${client}"
         fi
  done

printf "$LIST"
}
##########################################

LIST_CLIENT=$(list_client)
 
for d in $LIST_CLIENT; do
   d_name=`docker inspect -f {{.Name}} $d`
    printf "$d_name: "
    #calculer la taille de dossier /var/lib/docker/containers de chaque docker
    sudo du -d 2 -h /var/lib/docker/containers | grep `docker inspect -f "{{.Id}}" $d | awk '{print $1}'` | awk '{print $1}' > xx
   
    # calculer la taille de docker du dossier /var/lib/docker/overlay2
    du -sh $(docker inspect --format='{{json .GraphDriver.Data.WorkDir}}' $d | awk -F/ '{print "/"$2"/"$3"/"$4"/"$5"/"$6}') | awk '{print $1}'  >> xx  
    #boucle pour calculer la taille des volumes de docker
    for mount in `docker inspect -f "{{range .Mounts}} {{.Source}}:{{.Destination}}{{end}}" $d`; do
	size=`echo $mount | cut -d':' -f1 | sudo xargs du -d 0 -h | awk '{print $1}'`
        echo $size >> xx
       
    done
   function_name xx

done

rm xx
